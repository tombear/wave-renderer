import H from "callbag-html";

export const ui = state => {

  const { canvasCtx: { canvas }, playbackRate, gain, emitter } = state;

  canvas.style.border = "1px solid black";
  canvas.style.width = "100%";
  canvas.style.height = "auto";

  return H.div({
    style: {
      display: "flex",
      flexFlow: "column",
      width: "calc(100%-3em)",
      padding: "1em"
    }
  }, [
    canvas,
    range({
      min: 0.01,
      max: 10.00,
      step: 0.01,
      id: "playbackRate",
      oninput: ev => emitter({ playbackRate: ev.target.value }),
      value: playbackRate,
    }),
    range({
      min: 0.00,
      max: 1.00,
      step: 0.01,
      id: "gain",
      oninput: ev => emitter({ gain: ev.target.value }),
      value: gain,
    })
  ])
}

const drawModeToLabel = drawMode => ({
  "DRAW": "Draw",
  "MOVE": "Move",
})[drawMode]

const idToFriendly = id => ({
  "playbackRate": "Frequency",
  "gain": "Gain"
})[id];

const range = ({ min, max, step, id, oninput, value }) => H.div([
  H.input({
    id,
    type: "range",
    min,
    max,
    step,
    oninput,
    value
  }),
  H.label(idToFriendly(id))
]);

function uncheckOthers(t) {
  Array.from(t.parentElement.parentElement.querySelectorAll("input[type=radio]"))
    .filter(x => x !== t)
    .forEach(x => x.checked = false)
}