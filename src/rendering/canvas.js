import Spline from "cubic-spline";

export const drawOnCanvas = state => {
  let { context, points, splineDistance } = state;
  const { canvas: { width, height }} = context;

  context.clearRect(0, 0, width, height);
  points.forEach(({ path }) => { if (path) context.stroke(path) } );
  context.beginPath();
  context.moveTo(0, height / 2);

  const xs = points.map(({ x }) => x);
  const ys = points.map(({ y }) => y);
  const spline = new Spline(xs, ys);

  for (let x = 1; x < width; x += splineDistance) {
    let y = spline.at(x);
    context.lineTo(x, y);
  }
  context.lineTo(width, height / 2);
  context.stroke();
}