import fromFunction from "callbag-from-function";
import H from "callbag-html";
import { asyncScheduler, BehaviorSubject, fromEvent, interval, of, zip } from "rxjs";
import { map, repeat, scan, share, startWith, switchMap } from "rxjs/operators";
import { ui } from "./rendering";
import { toObservable } from "./util";

export class WaveRenderer extends HTMLElement {

  static get observedAttributes() {
    return ["source-element-id"];
  }

  get sourceElementId() {
    return this.getAttribute("source-element-id");
  }

  set sourceElementId(v) {
    this.setAttribute("source-element-id", v);
  }

  constructor() {
    super();
    const shadowRoot = this.attachShadow({ mode: "closed" });
    shadowRoot.appendChild(H.style(`
    :host {
      display: flex;
      flex-flow: column;
      border: 0.1em solid black;
      width: 100%;
      min-width: 320px;
      max-width: 27em;
    }
    `));

    const { source, emitter } = fromFunction();
    const source$ = toObservable(source);

    const canvasCtx = H.canvas().getContext("2d");

    const initialState = {
      audioCtx: new AudioContext(),
      canvasCtx,
      playbackRate: 1.75,
      gain: 0.5,
      emitter,
      analyserInterval: 10
    }

    const { audioCtx } = initialState;
    this.audioContext = audioCtx;

    let bufSrc;
    const gainNode = audioCtx.createGain();
    const analyserNode = audioCtx.createAnalyser();
    analyserNode.fftSize = 2048;
    let bufferLength = analyserNode.frequencyBinCount;
    let dataArray = new Uint8Array(bufferLength);

    gainNode.gain.value = initialState.gain;
    gainNode.connect(analyserNode);
    analyserNode.connect(audioCtx.destination);

    const sourceElement = document.getElementById(this.sourceElementId);

    shadowRoot.appendChild(ui(initialState));

    const waveData =
      fromEvent(sourceElement, "waveData").pipe(
        map(({ detail: { waveData, height } }) => ({ waveData, height })),
        share()
      );

    const waveDataSubject = new BehaviorSubject()
    waveData.pipe(
      map(f),
    ).subscribe(waveDataSubject);

    function f({ waveData, height }) {
      let ys = waveData.map(([x, y]) => y);
      let buf = audioCtx.createBuffer(1, ys.length, 22050);
      for (let channel = 0; channel < buf.numberOfChannels; channel++) {
        let nowBuffering = buf.getChannelData(channel);
        for (let x = 0; x < ys.length; x++) {
          let y = ((ys[x] / (height / 2)) - 1) * -1;
          nowBuffering[x] = y;
        }
      }
      return buf;
    }

    const state$ = source$.pipe(
      startWith(initialState),
      scan((acc, v) => ({ ...acc, ...v }), initialState),
      share()
    );

    state$.pipe(
      switchMap(state => zip(waveDataSubject, of(state, asyncScheduler).pipe(repeat()))),
    ).subscribe(([buffer, { playbackRate, gain }]) => {
      if (bufSrc) bufSrc.disconnect();
      bufSrc = audioCtx.createBufferSource();
      bufSrc.playbackRate.value = 1.75;
      bufSrc.loop = true;
      bufSrc.buffer = buffer;
      bufSrc.playbackRate.value = playbackRate;
      gainNode.gain.value = gain;
      bufSrc.connect(gainNode);
      bufSrc.start();
    });

    state$.pipe(
      startWith(initialState),
      switchMap(({ analyserInterval }) => interval(analyserInterval)),
    ).subscribe(_ => {
      analyserNode.getByteTimeDomainData(dataArray);
      const { width, height } = canvasCtx.canvas;
      canvasCtx.clearRect(0, 0, width, height);
      let sliceWidth = width * 1.0 / bufferLength;
      let x = 0;

      canvasCtx.beginPath();
      for (let i = 0; i < bufferLength; i++) {

        let v = dataArray[i] / 128.0;
        let y = v * height / 2;

        if (i === 0) {
          canvasCtx.moveTo(x, y);
        } else {
          canvasCtx.lineTo(x, y);
        }

        x += sliceWidth;
      }
      canvasCtx.lineTo(width, height / 2);
      canvasCtx.stroke();
    })
  }
};

if (customElements && customElements.get("wave-renderer") === undefined)
  customElements.define("wave-renderer", WaveRenderer);