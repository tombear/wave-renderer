import { Observable } from "rxjs";

export function toObservable(source) {
  return new Observable(observer =>  {
    let talkback;
    try {
      source(0, (t, d) => {
        if (t === 0) talkback = d;
        if (t === 1) observer.next(d);
        if (t === 2 && d) observer.error(d);
        else if (t === 2) talkback = void 0, observer.complete(d);
      });
    } catch (err) {
      observer.error(err);
    }
    return function unsubscribe() {
      if (talkback) talkback(2);
    };
  });
}