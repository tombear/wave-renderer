import { switchMap, tap } from "rxjs/operators";

class SwitchScanOperator {
  constructor(accumulator, seed) {
    this.accumulator = accumulator;
    this.seed = seed;
  }
  call(subscriber, source) {
    let seed = this.seed;
    return source.pipe(switchMap((value, index) => this.accumulator(seed, value, index)), tap((value) => seed = value)).subscribe(subscriber);
  }
}

export function switchScan(accumulator, seed) {
  return (source) => source.lift(new SwitchScanOperator(accumulator, seed));
}