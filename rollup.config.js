import commonjs from "rollup-plugin-commonjs";
import nodeResolve from "rollup-plugin-node-resolve";

export default {
  input: "./src/index.js",
  output: {
    file: "./dist/wave-renderer.js",
    format: "esm",
    sourcemap: true,
    name: "WaveRenderer"
  },
  plugins: [
    nodeResolve(),
    commonjs(),
  ]
};